ip=$( minikube ip )
port=$( kubectl get services |  grep 'pi-service' | grep -oh ':.*/' |   awk '{print substr($0, 2, length($0) - 2)}')
url=http://$ip:$port/5000
echo $url
seq 1000 | xargs -P8 -Iz curl $url