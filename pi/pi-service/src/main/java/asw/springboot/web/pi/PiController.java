package asw.springboot.web.pi;

import java.util.Map; 

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;

import java.math.BigDecimal;


@RestController
public class PiController {

    @RequestMapping("/{pi}")
	public String hello(Map<String, Object> model, @PathVariable String pi) {
        Pi pi_greco = new Pi(Integer.parseInt(pi));
        BigDecimal result = pi_greco.execute();
		return result.toString();
	}
	
}
