#!/bin/bash

kubectl create -f metrics-server-master/deploy/1.8+
kubectl create -f configurations/services
kubectl create -f configurations/deployments
kubectl autoscale deployment pi-deployment --cpu-percent=20 --min=1 --max=5

