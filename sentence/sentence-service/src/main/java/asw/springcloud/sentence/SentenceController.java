package asw.springcloud.sentence;


import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;


import java.util.logging.Logger; 

@RestController
public class SentenceController {

	private final Logger logger = Logger.getLogger("asw.springcloud.sentence"); 
    
	@Value("${service.object}")
	private String objectServiceUrl;
	
	@Value("${service.subject}")
	private String subjectServiceUrl;
	
	@Value("${service.verb}")
	private String verbServiceUrl;
	
	
	@RequestMapping("/")
	public String getSentence() {
        RestTemplate restTemplate = new RestTemplate();
        String object = restTemplate.getForObject(objectServiceUrl, String.class);
        String verb = restTemplate.getForObject(verbServiceUrl, String.class);
        String subject = restTemplate.getForObject(subjectServiceUrl, String.class);
		logger.info("getSentence(): ");
		return subject+" "+ verb+" "+ object;
	}
	
}
