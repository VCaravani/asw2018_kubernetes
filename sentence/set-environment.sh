#!/bin/bash


eval $(minikube docker-env)

docker build -t object:v1 object-service/
docker build -t subject:v1 subject-service/
docker build -t verb:v1 verb-service/
docker build -t sentence:v1 sentence-service/
