#!/bin/bash

LOCAL_PATH=$(pwd)


minikube ssh "cd $LOCAL_PATH && docker build -t subject:v1 subject-service/"
minikube ssh "cd $LOCAL_PATH && docker build -t verb:v1 verb-service/"
minikube ssh "cd $LOCAL_PATH && docker build -t object:v1 object-service/"
minikube ssh "cd $LOCAL_PATH && docker build -t sentence:v1 sentence-service/"





