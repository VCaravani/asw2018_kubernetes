#!/bin/bash



export NODE_PORT=$(kubectl get services/$1-service -o go-template='{{(index .spec.ports 0).nodePort}}')
echo NODE_PORT=$NODE_PORT


for i in {1..10}; do 
	curl $(minikube ip):$NODE_PORT 
	echo "" ; 
done 