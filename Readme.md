# Sentence
## Esempio di applicazione a microservizi con Spring Boot e Kubernetes
Semplice applicazione per la generazione di frasi casuali composta da quattro microservizi: 
 
 * subject-service: restituisce un soggetto   
 
 * verb-service: restituisce un verbo
 
 * object-service: restituisce un complemento oggetto
 
 * sentence-service: sfrutta i tre servizi precedenti per comporre una frase

Tutti i servizi sono esposti pubblicamente e raggiungibili mediante chiamate REST.
Per ogni micro-servizio viene definito un servizio kubernetes e un relativo deployment. 

## Per iniziare


### Prerequisiti

Prima di iniziare hai bisogno di: 

* minikube: � un tool che semplifica l'esecuzione locale di kubernetes. Offre la possibilit� di eseguire un cluster kubernetes mono-nodo all'interno di una VM sulla macchina dell'utente.  
            Per le istruzioni per l'installazione riferirsi a https://kubernetes.io/docs/tasks/tools/install-minikube/.  
            Per ulteriori dettagli visitare https://github.com/kubernetes/minikube.  
            


* kubectl: una interfaccia su linea di comando per eseguire comandi sui cluster kubernetes.  
           Per le istruzioni per l'installazione riferirsi a https://kubernetes.io/docs/tasks/tools/install-kubectl/.  
           Per ulteriori dettagli visitare https://kubernetes.io/docs/reference/kubectl/overview/.  
           


* VirtualBox: per le istruzioni per l'installazione riferirsi a  https://www.virtualbox.org/.  

N.B. : � possibile utilizzare altri hypervisor oltre a VirtualBox come specificato nelle istruzioni di installazione di minikube

### Installazione

1. Per prima cosa � necessario clonare il repository: 


```
$ git clone https://bitbucket.org/VCaravani/asw2018_kubernetes/src
```

2. Posizionarsi dentro la cartella sentence e eseguire i comandi

```
$ minikube start 
$ sh deploy-sentence-service.sh
```

Il cluster dovrebbe essere stato creato e in running con tutti i servizi e i deployment necessari per l'applicazione. 

Puoi vericarlo utilizzando il comando: 


```
$ kubectl get pods 
NAME                                   READY     STATUS    RESTARTS   AGE
object-deployment-7d5497fbd4-bb2mh     1/1       Running   0          1h
sentence-deployment-7f54f7755d-b5wlb   1/1       Running   0          1h
subject-deployment-6c6c846f47-9jbgg    1/1       Running   0          1h
verb-deployment-7887776d9d-jtsp6       1/1       Running   0          1h
```

N.B. : i pod possono impiegare alcuni minuti per diventare ready. 

Oltre all'interfaccia offerta da kubectl minikube mette a disposizione una dashboard, accessibile da: 

```
$ minikube dashboard
```


## Accedere ai servizi

Per accedere al servizio nome-servizio digitare il comando: 


```
$ minikube service nome-servizio
```

Verrai reindirizzato al servizio mediante il tuo browser di default.

N.B. : Per adesso non funziona (inspiegabilmente) con Microsoft Edge.


## Come ottenere le immagini Docker per i deployment

Minikube contiene al suo interno un demone Docker. Non � quindi necessario avere Docker sulla propria macchina per buildare delle nuove immagini. 

Esistono due possibilit�: 

* Utilizzare il demone interno di Docker come descritto in https://kubernetes.io/docs/tutorials/hello-minikube/ nella sezione "Create a Docker container image". 
   
N.B.: Sui sistemi windows il metodo descritto non funziona (il comando eval non restituisce il risultato desiderato, ovvero l'host non riesce a puntare al Docker Deamon interno alla VM di minikube).  
      E' comunque sempre possibile connettersi direttamente al terminale minikube utilizzando il comando:   
         
        
```
$ minikube ssh  
```
         
   per poi eseguire direttamente i comandi Docker per la build.  
   Sono presenti degli script di esempio per la configurazione dell'ambiente per Linux, MacOs, Windows (set-evironment.sh, set-evironment-win.sh).  
 
         
         
         
* Buildare le immagini da una macchina contente Docker e caricarle su Docker Hub (il registry pubblico di immagini Docker, basta avere un account)
   come descritto in https://docs.docker.com/docker-cloud/builds/push-images/.   
   Le immagini su Docker Hub vengono automaticamente scaricate dal deployment specificando <nome_repository/nome_immagine:versione> nel campo image nei deployment.   
   Per un esempio � possibile consultare i file di deployment all'interno delle cartelle  di configurazione.   

## Eseguire il rolling-update dell'applicazione 
Il rolling-update applica modifiche alle configurazioni dei pod, queste modifiche vengono gestite da un replication controller.
E' possibile aggiornare l'applicazione specificando direttamente una nuova immagine del contenitore, oppure utilizzando un file di configurazione.
Nel nostro caso pu� essere eseguito l'aggiornamento dei diversi servizi tramite uno script specificando il servizio da aggiornare e il numero della versione.

```
$ sh update-version <nome-servizio> <versione>

```


## Eseguire il roll-back dell'applicazione 
Kubernetes offre la possibilit� di effettuare il roll-back ad una versione precendente dell'applicazione.

Per eseguire il roll-back:
```

$ sh rollback-version <nome-servizio> <versione>
```
   
N.B.: sono presenti su Docker Hub le versioni 2 (v2) per ogni servizio dell'applicazione. I comandi kubectl per il rolling update e il rollback sono visibili
negli script. 


# Hello Service
## Applicazione mono-servizio di esempio per verificare la scalabilit� e il load balancing
Una piccola applicazione di esempio che pu� essere eseguita sul cluster kubernetes di minikube 
seguendo i passi gi� spiegati (gli script di deploy e cleaning si chiamano in modo simile a quelli di sentence). 
E' composta da un solo servizio replicato (le repiliche di default sono 5) a cui vengono effettuate pi� richieste per poter verificare che la risposta 
arrivi da repliche differenti. 
Dopo aver effettuato il deploy � possibile verificare il corretto funzionamento eseguendo il comando: 

```
$ sh run-curl-service-host.sh hello-service | grep -oh 'hello-deployment-.*!' | awk '{count[$1]++} END {for (service in count) print service, count[service]}'
 
hello-deployment-95dc9f4c5-vwm2t! 22
hello-deployment-95dc9f4c5-4m2vw! 17
hello-deployment-95dc9f4c5-xqt27! 20
hello-deployment-95dc9f4c5-k2z2d! 23
hello-deployment-95dc9f4c5-pvggm! 18

```
Le cifre vicino ai nomi dei pod indicano il numero di richieste arrivategli.  
Il Servizio hello-service creato nel deploy funge da endpoint e si occupa di effettuare il load balancing delle richieste alle
diverse repliche. 

# Pi Service
## Applicazione mono-servizio di esempio per verificare la funzionalit� di autoscaling orizzontale dei servizi Kubernetes
Una piccola applicazione di esempio che pu� essere eseguita sul cluster kubernetes di minikube 
seguendo i passi gi� spiegati (gli script di deploy e cleaning si chiamano in modo simile a quelli di sentence). 
La funzione dell'applicazione � quella di calcolare il pi-greco con una precisione, espressa nel numero di cifre, stabilita dal client al momento della richiesta. 
E' composta da un solo servizio la cui scalabilit� � gestita tramite la funzionalit� di autoscaling orizzaontale messa a disposizione nativamente da kubernetes 
e descritta a https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/#what-is-the-horizontal-pod-autoscaler. 
Tale funzionalit� pu� essere attivata eseguendo il comando: 

```
$ kubectl autoscale deployment pi-deployment --cpu-percent=20 --min=1 --max=5
```

L'opzione --cpu-percent indica la quantit� desiderata di utilizzo medio della cpu nei pod.  
Le opzioni --min e --max indicano rispettivamente il numero minimo e massimo di repliche ammesse.  

N.B. : il comando viene eseuito nello script di deployment. 

Come descritto nella documentazione � necessaria la presenza di un servizio, il Metrics-Server, per monitorare l'utilizzo di risorse nei diversi pod. 
Il codice del metrics-server utilizzato pu� essere trovato a https://github.com/kubernetes-incubator/metrics-server. 

N.B.: sono qui presenti solamente i file di configurazione necessari per il deploy del server, che viene effettuato nello script di avviamento. 

Per verificare il corretto funzionamento dell'applicazione eseguire lo script:   

```
$ sh run_concurrently_requests.sh

```

Lo script esegue richieste multiple (di default sono mille con una precisione di 5000 cifre) al servizio su diversi thread, stressando la cpu dei pod
e provocando l'autoscaling del servizio. 
E'possibile, durante l'esecuzione dello script, monitorare il servizio attraverso il comando: 

```
$ kubectl get hpa 
NAME            REFERENCE                  TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
pi-deployment   Deployment/pi-deployment   0%/20%    1         5         1          41m


$ kubectl get hpa
NAME            REFERENCE                  TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
pi-deployment   Deployment/pi-deployment   99%/20%   1         5         1          46m


$ kubectl get hpa
NAME            REFERENCE                  TARGETS   MINPODS   MAXPODS   REPLICAS   AGE
pi-deployment   Deployment/pi-deployment   99%/20%   1         5         4          46m
```

